﻿CREATE TABLE [Metadata].[tbl_RunDate](
	[RunDate] [datetime2](7) NULL,
	[RunDateEnd]  AS (dateadd(millisecond,datediff(millisecond,'00:00:00.000','23:59:59.999'),[RunDate])),
	[CNTRY_C] [varchar](100) NULL
) ON [PRIMARY]

GO